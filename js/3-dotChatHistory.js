/*
 * Author: Thijs Kosterman
 * Creation Date: 28-02-2020
 */


// will be filled with user input and bot responses
var chatHistoryArray = []

function getChatHistory() {
    return chatHistoryArray;
}

// add to history. Loaded items should be ignored when filling the chatarea, but
// this functionality is currently removed
function addToChathistory(content, position, loaded) {
    if (typeof loaded === 'undefined' || loaded === null) {
        loaded = false;
    }

    var newChat = {
        "position": position,
        "content": content,
        "loaded": loaded,
    }

    chatHistoryArray.push(newChat);
    window.localStorage.setItem('chatHistory', JSON.stringify(getChatHistory()));
}