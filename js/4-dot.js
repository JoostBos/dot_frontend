/*
 * Author: Thijs Kosterman
 * Creation Date: 28-02-2020
 */

var config = getConfig();
var chatHistory = getChatHistory();
var content = getEnglishContent();

if (config.lan === 'nl') {
    content = getDutchContent();
} 

// set sizes of chatbox (like height) dynamically on function call
function setChatbox(excludeInputArea, keepHidden, smooth) {
    var chatbox = $('#dot.dot-chatbot .dot-chatbox');
    var header = $('#dot.dot-chatbot .dot-chatbox-header');
    var subheader = $('#dot.dot-chatbot .dot-chatbox-subtitle');
    var input = $('#dot.dot-chatbot .dot-input-area');
    var heightToSubtract = header.height() + input.height() + subheader.height();

    if (typeof excludeInputArea !== 'undefined' && excludeInputArea === true) {
        heightToSubtract -= input.height() - 20;
    }

    if (typeof keepHidden === 'undefined') {
        keepHidden = false
    }

    if (typeof smooth === 'undefined') {
        smooth = false
    }

    var mainHeight = chatbox.height();
    var newHeight = mainHeight - heightToSubtract;
    if (($('#dot.dot-chatbot .dot-chatbox').is(':hidden') && !keepHidden) || !$('#dot.dot-chatbot .dot-chatbox').is(':hidden')) {
        if (smooth) {
            $('#dot.dot-chatbot .dot-chatbox-chatarea, #dot.dot-chatbot .dot-input-area').css({ 'opacity': '0' });
            setTimeout(function () {
                $('#dot.dot-chatbot .dot-chatbox-chatarea').animate({ 'opacity': '1' });
                $('#dot.dot-chatbot .dot-input-area').animate({ 'opacity': '1' });
            }, 600);
        }

        $('#dot.dot-chatbot .dot-chatbox').css({ 'height': '70%' });
        $('#dot.dot-chatbot .dot-chatbox').show();
    }

    $('#dot.dot-chatbot .dot-chatbox-chatarea').css({ 'height': newHeight });
    $('.dot-chatbox-chatarea').scrollTop($('.dot-chatbox-chatarea')[0].scrollHeight);

    if (!isMobile()) {
        $('#dot.dot-chatbot input').focus();
    }
}

function getCurrentDate() {
    var d = new Date();

    var month = d.getMonth() + 1;
    var day = d.getDate();

    var output = (day < 10 ? '0' : '') + day + '-' +
        (month < 10 ? '0' : '') + month + '-' + d.getFullYear();

    return output;
}

function isMobile() {
    try {
        document.createEvent("TouchEvent");
        return true;
    }
    catch (e) {
        return false;
    }
}