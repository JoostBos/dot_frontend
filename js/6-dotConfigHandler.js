/*
 * Author: Thijs Kosterman
 * Creation Date: 28-02-2020
 */

// show all elements hidden on prod when de env is set to dev
$('[data-env]').each(function () {
	if ($(this).data('env') === 'dev' && config.env === 'dev') {
		$(this).show();
	}
});

// get all content from language handlers
$('[data-content]').each(function () {
	$(this).html(content[$(this).data('content')]);
});

$('[data-content-input-placeholder]').each(function () {
	$(this).attr('placeholder', content[$(this).data('content-input-placeholder')]);
});