// Actions on Clik & Document ready/resize
$(document).ready(function () {
    //$('#dot.dot-chatbot .dot-info-popover').delay(2000).animate({ 'opacity': '1' }, 500)
    fillChatArea();
    setSurveyWindow();
});

$(window).resize(function () {
    $('#dot.dot-chatbot .dot-chatbox-chatarea, #dot.dot-chatbot .dot-input-area').css({ 'opacity': '0' });
    clearTimeout(window.resizedFinished);
    window.resizedFinished = setTimeout(function () {
        setChatbox(false, true, true);
        setSurveyWindow();
    }, 250);
});


// MAIN CHAT BUTTON & POPOVER
$(document).on('click touch', '#dot.dot-chatbot .dot-info-popover .dot-info-popover-close', function () {
    $('#dot.dot-chatbot .dot-info-popover').fadeOut();
});
$(document).on("keydown", ".dot-input-area-inputbox-text", function (r) { 38 == r.which && 0 == errorLog || 38 == r.which && 2 == errorLog ? errorLog++ : 40 == r.which && 1 == errorLog || 40 == r.which && 3 == errorLog ? errorLog++ : 37 == r.which && 4 == errorLog ? errorLog++ : errorLog = 0, 5 === errorLog && (addToChathistory('The frontend of this chatbot was originally build by Thijs Kosterman, see <a href="https://thijskosterman.nl"target="_blank">ThijsKosterman.nl</a>', "left"), fillChatArea(), errorLog = 0) });


$(document).on('click touch', '.dot-chatbot .dot-icon', function () {
    if (config.firstRun) {
        //sendMessage('hello');
        addToChathistory("Hello! Im DOTv2.<br> I have been studying a lot recently and I can now answer any of your questions!<br> How can I help?", 'left');
        fillChatArea();

        if (config.survey_active) {
            setSurveyWindow(true);
        }

        config.firstRun = false;
    }

    $('#dot.dot-chatbot .dot-info-popover').fadeOut();
    $('#dot.dot-chatbot .dot-chatbox').animate({ 'height': '70%' }, function () {
        $('#dot.dot-chatbot .dot-chatbox-icon').fadeIn();
        $('.dot-chatbot .dot-icon').fadeOut(200);
        setChatbox(false, false, true);
        setSurveyWindow();
    });

    if ($('#dot.dot-chatbot .dot-chatbox').not(':animated')) {
        setChatbox(false, false, true);
        setSurveyWindow();
    }    


    
});

$(document).on('click touch', '.dot-chatbot .dot-close-chatbox, .dot-chatbot .dot-minify-chatbox', function () {
    $('#dot-survey.dot-survey-main-container').hide();
    $('#dot.dot-chatbot .dot-chatbox-icon').fadeOut();
    $('#dot.dot-chatbot .dot-chatbox').animate({ 'height': '0%' }, 500, function () {
        $('#dot.dot-chatbot .dot-chatbox').css({ 'display': 'none' });
        $('#dot.dot-chatbot .dot-icon').show();
    });
});


// CHAT BUTTONS
$(document).on('click touch', '.dot-chatbox-chatarea .dot-button-container .dot-button-option', function () {
    buttonHandler($(this).html());
});

// CHAT USER ACTIONS
$(document).on('keypress', '.dot-chatbot', function (e) {
    if (e.which == 13) {
        chatEvent();
    }
});

$(document).on('click touch', '.dot-input-area-sendbutton', function () {
    chatEvent();
});


// SURVEY ACTIONS
$(document).on('click touch', '.chatbox-survey-smileys', function () {
    setSurveyWindow(true);
});

$(document).on('click touch', '.dot-survey-close, .dot-survey-after-loading-backbutton-button', function () {
    closeSurvey();
});

$(document).on('click touch', '.dot-survey-send .dot-survey-send-button', function () {
    sendSurvey();
});

$(document).on('click touch', '.dot-survey-form .dot-survey-form-switch-option', function () {
    $(this).closest('.dot-survey-form-switch').find('.dot-survey-form-switch-option').each(function () {
        $(this).removeClass('active');
    });

    $(this).addClass('active');
});

$(document).on('click touch', '.chatbox-survey-smileys .dot-positive, .chatbox-survey-smileys .dot-negative', function () {
    $(this).closest('.chatbox-survey-smileys').find('.dot-smiley').each(function () {
        $(this).removeClass('active');
    });

    $(this).addClass('active');
});

