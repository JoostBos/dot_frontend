/*
 * Author: Thijs Kosterman
 * Creation Date: 28-02-2020
 */

// fill html with following text by using data attribute 'content' with the respective key as value
function getDutchContent() {
  return {
      "info-popover":               "<strong>Hi, mijn naam is Dot. </strong><div class='Dot-content-spacer'></div> Wat kan ik voor je betekenen?",
      "chatbox-header":             "<strong>D</strong>eloitte <strong>O</strong>nline <strong>T</strong>alent Assistant",
      "chatbox-subtitle":           "<strong>Hi, mijn naam is Dot. </strong><div class='Dot-content-spacer'></div> Wat kan ik voor je betekenen?",
      "user-input":                 "Je vraag of bericht...",
      "survey-area":                "Jouw ervaring met Dot:",
      "survey-area-send":           "Bedankt voor je feedback!",
      "survey-area-close":          "Terug naar chat",
      "survey-header-after":        "Dot Experience Survey",
      "survey-content-header-pt1":  "Leuk dat je met Dot hebt gesproken!",
      "survey-content-header-pt2":  "We horen graag wat je ervan vond en met jouw feedback kunnen we Dot nog beter maken.",
      "survey-content-header-pt3":  "Vertel ons je ervaring:",
      "survey-rating":              "Jouw ervaring:",
      "survey-answer":              "Zijn je vragen beantwoord?",
      "survey-answer-yes":          "Ja",
      "survey-answer-no":           "Nee",
      "survey-rating-expl-left":    "Slecht",
      "survey-rating-expl-right":   "Goed",
      "survey-input":               "Wat zou beter kunnen?",
      "survey-button":              "Verstuur",
      "survey-sending":             "Je feedback wordt verstuurd...",
      "survey-success-text":        "Je feedback is verstuurd, bedankt voor je mening!",
      "teneo-error":                "Er is iets misgegeaan, probeer het opnieuw!",
      "back":                       "Terug",
  }
}


function getEnglishContent() {
  return {
      "info-popover":               "<strong>Hi, I'm Dot. </strong><div class='Dot-content-spacer'></div> How can I help you today?",
      "chatbox-header":             "<strong>D</strong>eloitte <strong>O</strong>nline <strong>T</strong>alent Assistant",
      "chatbox-subtitle":           "<strong>Hi, I'm Dot. </strong><div class='Dot-content-spacer'></div> How can I help you today?",
      "user-input":                 "Type your message...",
      "survey-area":                "Your experience with Dot:",
      "survey-area-send":           "Thanks for your feedback!",
      "survey-area-close":          "Back to chat",
      "survey-header-after":        "Dot Experience Survey",
      "survey-content-header-pt1":  "Thanks for using Dot!",
      "survey-content-header-pt2":  "With your feedback we can make Dot work even better.",
      "survey-content-header-pt3":  "Please let us know your thoughts below:",
      "survey-rating":              "Your rating:",
      "survey-answer":              "Were your questions answered?",
      "survey-answer-yes":          "Yes",
      "survey-answer-no":           "No",
      "survey-rating-expl-left":    "Bad",
      "survey-rating-expl-right":   "Great",
      "survey-input":               "Please describe what you think can be improved:",
      "survey-button":              "Send",
      "survey-sending":             "Sending your input...",
      "survey-success-text":        "We received your feedback, thanks for your opinion!",
      "teneo-error":                "Something went wrong, please try again!",
      "back":                       "Back",
  }
}