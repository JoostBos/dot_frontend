/*
 * Author: Thijs Kosterman
 * Creation Date: 28-02-2020
 */


// config variables
// env & lan will be automatically changed when DotBuilder is used
// changing lan to 'en' or 'nl' wil change the language to english or dutch respectively
var configObj = {
    "env": "dev",
    "lan": "en",
    "token": "CUyhrOwHgl6DtvtbZEJ7HrruKW0woJ2qqczaKMgGVTEAR55eHTEUSocrWAFJnOIOmHtDO1gJwOT4Vg3o",
    "viewtype": "tieapi", 
    "request_url": "https://arria.azurewebsites.net/del/del.php",
    "endpoint_en_dev": "https://epiccondors-b86506.bots.teneo.ai/nqa_test_deloitt_6b2vwp3mpx8zqr3c90ccx2jrr5/",
    "endpoint_en_dev_old": "https://deloitte-netherlands-dot-gben-qa.teneocloud.com/deloitte-dot-va-1/ ",
    "endpoint_en_prod": "https://deloitte-netherlands-dot-gben.teneocloud.com/deloitte-dot-va-1/ ",
    "endpoint_nl_dev": "https://epiccondors-b86506.bots.teneo.ai/nqa_test_deloitt_6b2vwp3mpx8zqr3c90ccx2jrr5/",
    "endpoint_nl_prod": "https://deloitte-netherlands-dot-nlnl.teneocloud.com/deloitte-dot-va-2/",
    "firstRun": true,
    "survey_active": false,
}

function getConfig() {
    return configObj;
}

// get the right API endpoint based on the current language
function getTeneoEndpoint() {
    var config = getConfig();

    return config['endpoint_' + config.lan + '_' + config.env];
}