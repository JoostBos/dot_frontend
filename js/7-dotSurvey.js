/*
 * Author: Thijs Kosterman
 * Creation Date: 28-02-2020
 */

var whiteStar = '&#9733;';
var blackStar = '&#9734;';

var rating;
var errorLog = 0;

function survery(rating) {
    var starContainer = $('.dot-survey-form .dot-survey-area-rating');
    var whiteStarTemplate = '<span>' + whiteStar + '</span>';
    var blackStarTemplate = '<span>' + blackStar + '</span>';
    $(starContainer).html('');

    for (var i = 0; i < rating; i++) {
        starContainer.append(whiteStarTemplate);
    }

    for (var i = 0; i < (5-rating); i++) {
        starContainer.append(blackStarTemplate);
    }
}

function setSurveyWindow(showWindow) {
    if (typeof showWindow === 'undefined') {
        showWindow = false;
    }

    var chatbox = $('#dot.dot-chatbot .dot-chatbox');
    var header = $('#dot.dot-chatbot .dot-chatbox-header');
    var subheader = $('#dot.dot-chatbot .dot-chatbox-subtitle');
    var subheaderh = $('#dot.dot-chatbot .dot-chatbox-subtitle .dot-chatbox-subtitle-header-container');
    var height = chatbox.height() - (header.height() + subheader.height()) + (subheaderh.height() * 1.1);

    if (window.innerHeight <= 600) {
        height = height - 60
    }


    if (showWindow || config.survey_active) {
        config.survey_active = true;
        $('.dot-survey-main-container').show().animate({ 'height': height });
    }
 
    $('.dot-survey-main-container')
        .find('.dot-survey-form-switch').each(function () {
            var targetWidth = 0;
            var elements = $(this).find('.dot-survey-form-switch-option').length;
            var count = 1;
            var parent = $(this);

            $(this).find('.dot-survey-form-switch-option').each(function () {
                if (count === elements) {
                    $(parent).find('.dot-survey-form-switch-option').each(function () {
                        $(this).width(targetWidth);
                    })
                } else if (targetWidth === 0 || $(this).width() > targetWidth) {
                    targetWidth = $(this).width();
                }

                count++;
            })
        });
}


function closeSurvey() {
    $('.dot-survey-main-container').animate({ 'height': '0' }, 400);

    $('#dot.dot-chatbot').find('.chatbox-survey-smileys').each(function () {
        $(this).find('.dot-smiley').each(function () {
            $(this).removeClass('active');
        });
    });

    config.survey_active = false;
}

function sendSurvey() {
    $('#dot-survey.dot-survey-main-container .dot-survey-before, .dot-survey-area-before-send').hide();
    $('#dot-survey.dot-survey-main-container .dot-survey-after, .dot-survey-area-after-send').show();

    setTimeout(function () {
        var message = getSurveyCompatibleMessage(getSurveyData());
        $.ajax({
            url: 'https://arria.azurewebsites.net/del/del.php',
            type: 'POST',
            data: {
                'token': config.token,
                'endpoint': getTeneoEndpoint(),
                'JSESSIONID': sessionId,
                'body': 'userinput=' + encodeURIComponent(message) + '+&viewtype=' + config.viewtype + '&sessionId=' + sessionId
            },
            success: function (data) {
                showSurveyResponse(data);
                $('.dot-survey-after-loading').hide();
                $('.dot-survey-after-success').css({ 'display': 'table' });
            },
            error: function (xhr, err) {
                console.error('Del/Teneo communication failed: ' + err);
            }
        });
    }, 2500);
}

function getSurveyData() {
    return {
        giSurveyRating: getActiveSwitchValue('.dot-survey-rating-switch'),
        gbQuestionAnswered: getActiveSwitchValue('.dot-survey-question-switch'),
        gsUserFeedback: $('.dot-survey-full-width-subject-textarea textarea').val(),
        gsSurveyInputTimeStamp: getCurrentDate(),
    }
}

function getActiveSwitchValue(switchClass) {
    var elem = $('.dot-survey-form-switch' + switchClass);
    var value;

    elem.find('.dot-survey-form-switch-option').each(function () {
        if ($(this).hasClass('active')) {
            value = $(this).data('switch-value');
            return true;
        }
    });

    return value;
}

function getSurveyCompatibleMessage(surveyDataObject) {
    var trigger = 'survey_capture;';

    var message = trigger;
    for (var item in surveyDataObject) {
        message += item + ';' + surveyDataObject[item] + ';';
    }

    return message;
}

function showSurveyResponse(responseString) {
    var responseText = '';


    if (typeof responseString !== 'undefined' && responseString !== '') {
        var response = JSON.parse(responseString)
        var responseHeader = JSON.parse(response.response)
        responseText = responseHeader.output.text;
    }

    if (responseText === '') {
        responseText = content["teneo-error"];
    }

    $('.dot-survey-after-loading-successtext').html(responseText);
}