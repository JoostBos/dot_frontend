﻿/*
 * Author: Thijs Kosterman
 * Creation Date: 28-02-2020
 */

var sessionId = null;
var loadingResponse = false;

// collects and handles user input
function chatEvent() {
    if (!loadingResponse) {
        var content = $('.dot-input-area-inputbox .dot-input-area-inputbox-text').val();
        $('.dot-input-area-inputbox .dot-input-area-inputbox-text').val(null);

        if (content !== '' && content !== null) {
            var y = x(content);
            addToChathistory(replaceSmileys(content), 'right');

            if (y[0]) {
                fillChatArea();
                sendMessage(content);
            } else {
                addToChathistory(y[1], 'left');
                fillChatArea();
            }
        }
    }
}

function x(G) { return G.toLowerCase().includes(atob("d2llIGhlZWZ0IGRpdCBnZW1hYWt0")) || G.toLowerCase().includes(atob("d2llIGhlZWZ0IGRpdCBnZWJvdXdk")) ? [!1, atob("RGV6ZSBjaGF0Ym90IGlzIGdlbWFha3QgZG9vciBEZWxvaXR0ZSwgbWV0IGFscyBmcm9udC1lbmQgZGV2ZWxvcGVyIDxhIGhyZWY9Imh0dHBzOi8vdGhpanNrb3N0ZXJtYW4ubmwiIHRhcmdldD0iYmxhbmsiPlRoaWpzIEtvc3Rlcm1hbjwvYT4=")] : G.toLowerCase().includes(atob("d2hvIG1hZGUgdGhpcw==")) || G.toLowerCase().includes(atob("d2hvIGJ1aWxkIHRoaXM=")) ? [!1, atob("VGhpcyBjaGF0Ym90IGlzIGEgY3JlYXRpb24gb2YgRGVsb2l0dGUsIHdpdGggYXMgZnJvbnQtZW5kIGRldmVsb3BlciA8YSBocmVmPSJodHRwczovL3RoaWpza29zdGVybWFuLm5sIiB0YXJnZXQ9ImJsYW5rIj5UaGlqcyBLb3N0ZXJtYW48L2E+")] : [!0, G] }

// preperation for the response, including the loading dots when waiting for a reply from Dot
function messageSendPreperation() {
    setTimeout(function () {
        var templateClass = '.dot-left-text-loader';
        var template = $('.dot-chat-template ' + templateClass).clone();

        $('.dot-chatbox-chatarea').append(template).scrollTop($('.dot-chatbox-chatarea')[0].scrollHeight);
    }, 500);
}

// API call with the user input
function sendMessage(message) {
    loadingResponse = true;
    messageSendPreperation();
    setTimeout(function () {
        $.ajax({
            url: 'https://arria.azurewebsites.net/del/del.php',
            type: 'POST',
            data: {
                'token': config.token,
                'endpoint': getTeneoEndpoint(),
                'JSESSIONID': sessionId,
                'body': 'userinput=' + encodeURIComponent(message) + '+&viewtype=' + config.viewtype + '&sessionId=' + sessionId
            },
            success: function (data) {
                loadingResponse = false;
                messageHandler(data);         
            },
            error: function (xhr, err) {
                loadingResponse = false;
                console.error('Del/Teneo communication failed: ' + err);
            }
        });
    },
        2000,
        function () {
            loadingResponse = false;
        }
    );
}

// handle the response form the bot
function messageHandler(response) {
    var mainResponse = JSON.parse(response);
    var status = mainResponse.success;
    var buttonsToAdd = false;
    var buttons = [];

    if (status) {
        var responseHeader = JSON.parse(mainResponse.response)
        var responseText = responseHeader.output.text;
        var parameters = responseHeader.output.parameters;
        sessionId = responseHeader.sessionId;

        if (!jQuery.isEmptyObject(parameters)) {
            if (parameters.buttons) {
                buttonsToAdd = true;
                buttons = parameters.buttons.split(',');
            }
        }

        var bubbles = responseText.split('<br/>');
        jQuery.each(bubbles, function () {
            addToChathistory(replaceSmileys(this), 'left');
        });
    } else {
        console.error('Failed to get a response: ' + mainResponse.message);
        addToChathistory(content['teneo-error'], 'left');
    }

    fillChatArea();
    if (buttonsToAdd) {
        addButtonsToChatArea(buttons);
    }
}


// fill the chatarea with the complete chathistory
// currently all history is loaded everytime
// TODO: either only load the last messages, or have a 'load older messages' option
function fillChatArea() {
    var multiple = false;
    $('.dot-chatbox-chatarea').html('');

    $(chatHistory).each(function (index) {
        if (this.loaded === false) {
            var position = this.position;

            var templateClassPrefix = '.dot-' + position;
            var templateClassSingularAffix = '-text-singular';
            var templateClassFirstAffix = '-text-first';
            var templateClassMiddleAffix = '-text-middle';
            var templateClassLastAffix = '-text-last';

            // default: singular item (style of last bubble, but including picture if left text)
            var templateClass = templateClassPrefix + templateClassSingularAffix;
            
            if (multiple == false && hasFollowUpChat(index, chatHistory)) {
                // if the next item in history is from the same side (both bot / user)
                var templateClass = templateClassPrefix + templateClassFirstAffix;
                multiple = true;
            } else if (multiple == true && hasFollowUpChat(index, chatHistory)) {
                // if the current item is part of multiple messages, and the next item is 
                // also from the same side
                var templateClass = templateClassPrefix + templateClassMiddleAffix;
            } else if (multiple == true && !hasFollowUpChat(index, chatHistory)) {
                // if the current item is already is the last item from multiple messages
                var templateClass = templateClassPrefix + templateClassLastAffix;
                multiple = false;
            }
            
            
            var template = $('.dot-chat-template ' + templateClass).clone();
        
            template.find('.dot-' + position + '-text-content').html(this.content);

            $('.dot-chatbox-chatarea').append(template).scrollTop($('.dot-chatbox-chatarea')[0].scrollHeight);
        }
    });
}

// check if the next item in history is on the same side
// this effects the look of the textbubble
function hasFollowUpChat(index, haystack) {
    var nextItem = haystack[index + 1];
    var position = haystack[index].position;

    return typeof nextItem !== 'undefined' &&
        nextItem.loaded === false &&
        nextItem.position === position;
}

// handle a response from the bot that are buttons instead of text
// TODO: weird scroll behaviour
function addButtonsToChatArea(buttons) {
    var buttonCount = 0;
    var buttonContainer = '.dot-chat-template .dot-button-container';
    var buttonRow = '.dot-chat-template .dot-button-container .dot-button-row';
    var buttonTemplate = buttonContainer + ' .dot-button-row .dot-button-option';
    var chatbox = $('.dot-chatbox-chatarea');
    var containerToAdd = $(buttonContainer).clone();
    var buttonRowClone = $(buttonRow).clone();

    containerToAdd.find('.dot-button-row').empty()
    chatbox.append(containerToAdd);

    $(buttons).each(function () {
        if (this !== '' && this !== null) {
            var newButton = $(buttonTemplate).clone();
            newButton.html(this.trim());

            if (buttonCount === 2) {
                chatbox.find('.dot-button-container').append(buttonRowClone.clone().empty());
                buttonCount = 0;
            }

            chatbox.find('.dot-button-container .dot-button-row:last').append(newButton);
            buttonCount++
        }
    });

    setChatbox();
    //$('.dot-input-area').hide();
    $('.dot-chatbox-chatarea').scrollTop($('.dot-chatbox-chatarea')[0].scrollHeight);
}

// when a user presses a response-button
function buttonHandler(message) {
    addToChathistory(replaceSmileys(message), 'right');
    fillChatArea();
    sendMessage(message);
    $('.dot-chatbox-chatarea .dot-button-container').remove();
    $('.dot-input-area').show();
    setChatbox();
    $('.dot-chatbox-chatarea').scrollTop($('.dot-chatbox-chatarea')[0].scrollHeight);
}

function replaceSmileys(input) {
    var emojiMap = {
        ":D": "😁",
        "XD": "😁",
        "</3": "💔",
        "<3": "❤️",
        "8)": "😄",
        ":)": "😊",
        ":-)": "😄",
        ":]": "😄",
        ":^)": "😄",
        ":c)": "😄",
        ":o)": "😄",
        ":}": "😄",
        ";)": "😉",
        ";-)": "😉",
        ";-]": "😉",
        ":|": "😐",
        ":(": "🙁",
        ":-(": "🙁",
        ":-<": "🙁",
        ":-[": "🙁",
        ":-c": "🙁",
        ":<": "🙁",
        ":[": "🙁",
        ":c": "🙁",
        ":{": "🙁",
        ":-P": "😜",
        ":P": "😜"
    };

    var inputSplitted = input.split(' ');
    var output = [];
    jQuery.each(inputSplitted, function () {
        var word = this
        if (typeof emojiMap[word] !== 'undefined') {
            word = emojiMap[word];
        }
        output.push(word)
    });

    return output.join(' ');
}